package com.maika.jfinal.dubbo.core;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import com.jfinal.core.Controller;
import com.jfinal.core.ControllerFactory;
import com.maika.jfinal.dubbo.annotation.ReferenceService;

public class ReferenceServiceAutowiredControllerFactory extends ControllerFactory {
	
	private ThreadLocal<Map<Class<? extends Controller>, Controller>> buffers = new ThreadLocal<Map<Class<? extends Controller>, Controller>>() {
		protected Map<Class<? extends Controller>, Controller> initialValue() {
			return new HashMap<Class<? extends Controller>, Controller>();
		}
	};
	@Override
	public Controller getController(Class<? extends Controller> controllerClass)
			throws InstantiationException, IllegalAccessException {
		Controller ret=buffers.get().get(controllerClass);
		if(ret==null){
			ret=controllerClass.newInstance();
			inject(ret);
			buffers.get().put(controllerClass, ret);
		}
		return ret;
	}
	private void inject(Controller controller) throws IllegalAccessException {
		Field[] fields=controller.getClass().getDeclaredFields();
		for (Field field : fields) {
			if(field.isAnnotationPresent(ReferenceService.class)){
				field.setAccessible(true);
				Object service = RpcServiceProxy.getService(field.getType());
				field.set(controller, service);
			}
		}
	}
	
}
