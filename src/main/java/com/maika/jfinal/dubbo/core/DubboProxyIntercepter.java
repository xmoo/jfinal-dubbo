package com.maika.jfinal.dubbo.core;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

public class DubboProxyIntercepter implements MethodInterceptor {
    public Object intercept(Object sub, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        String interfaceName = method.getDeclaringClass().getName();
        String methodName = method.getName();
        String address = "zookeeper://127.0.0.1:2181";
		String version = "1.0";
		List<Object> asList = Arrays.asList(objects);
		Object invoke = DubboCallbackUtil.invoke(interfaceName, methodName, asList, address, version);
        return invoke;
    }
}