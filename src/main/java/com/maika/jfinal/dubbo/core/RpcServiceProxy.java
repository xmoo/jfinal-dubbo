package com.maika.jfinal.dubbo.core;

import java.util.concurrent.ConcurrentHashMap;

import net.sf.cglib.proxy.Enhancer;

public class RpcServiceProxy {
	// 单例缓存
	protected static ConcurrentHashMap<Class<?>, Object> singletonCache = new ConcurrentHashMap<Class<?>, Object>();

	public static <T> T getService(Class<?> clz) {
		Object ret = null;
		synchronized (clz) {
			ret = singletonCache.get(clz);
			if (ret == null) {
				Enhancer enhancer = new Enhancer();
				enhancer.setSuperclass(clz);
				enhancer.setCallback(new DubboProxyIntercepter());
				ret = enhancer.create();
				singletonCache.put(clz, ret);
			}
			return (T)ret;
		}
	}
}
