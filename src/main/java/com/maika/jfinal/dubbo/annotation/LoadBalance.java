package com.maika.jfinal.dubbo.annotation;

public enum LoadBalance {
	RANDOM,ROUNDROBIN ,LEASTACTIVE ,CONSISTENTHASH ;
}
