# jfinal-dubbo

#### 介绍

jfinal-dubbo 是为了解决jfinal项目调用dubbo服务的功能

本项目基于
dubbo2.7+
jfinal4.3+

maven地址（并未提交到中央仓库），目前功能还比较简陋

```
		<dependency>
			<groupId>com.maika</groupId>
			<artifactId>jfinal-dubbo</artifactId>
			<version>0.0.1-SNAPSHOT</version>
		</dependency>
```
 


#### 使用说明

1. jfinal 项目的 JFinalConfig中配置
```
public void configConstant(Constants me) {
		loadConfig();
		me.setControllerFactory(new ReferenceServiceAutowiredControllerFactory());
		}
```
 
2. controller引用服务地方加注解
@ReferenceService

```
public class IndexController extends Controller {
	@ReferenceService
	DemoService myService;
	public void index() {
		render("index.html");
	}
	
	public void test() {
		renderText(myService.sayHello("hello"));
	}
}
```
